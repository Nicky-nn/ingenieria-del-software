# Ingeniería del Software



## Proceso para la implantación

### Plan de Implantación
* Definición del plna
* Equipo de implantación

#Competencia Específicas
* Estudia las caracteristicas del plan de implementacion de sistemas
* Distingue las didferencia entre los procesos de implamentacion de sistemas
* Diseña adecuadasmnete planes de implementacion de sisetmas
* Reasliza responsablemente las tareas y actividades de planificación


# Ingeneriaa deñl Software
* Requisitos
* Análisis
* Diseño
* Codificación
* Prueba

La IEEE Computer Society no define la implantación en mi copia de 2003 de su "Compilación de términos de ingeniería de software de fuentes existentes"(Tiene q fucnionar la implemnetacion -> Empresa) .



El IEEE define la implementación como "(1) El proceso de traducir un diseño en componentes de hardware, componentes de software o ambos. (2) El resultado del proceso en [IEEE Std. 610.12-1990]" (Programciuon de pequeños programas) . -> Programando

- [ ] Proyecto y proceso
# Proyecto(Todo) vs proceso(Modelos)
En pocas palabras, un proceso es un procedimiento establecido que implica una secuencia de pasos que deben tomarse para producir un resultado, mientras que un proyecto es un curso de acción temporal que tiene como objetivo entregar un producto, servicio o resultado distintivo.

| #1 Satisfaga a los clientes a través de una entrega temprana y continua |
| -------------------------- |
| La formulación original del primero de los principios Agile dice:  “ nuestra máxima prioridad es satisfacer al cliente mediante la entrega temprana y continua de software valioso ” . Sin embargo, es perfectamente |aplicable en áreas fuera del desarrollo de software. |

| #2 Cambios de requisitos incluso tarde en el proyecto |
| -------------------------- |
| Aún así, si es necesario, las solicitudes de cambio deberían ser bienvenidas incluso en las últimas etapas de la ejecución del proyecto. El texto original del segundo de los principios Agile establece que su equipo debe  "dar la bienvenida a los requisitos cambiantes, incluso al final del desarrollo. Los procesos ágiles aprovechan el cambio para la ventaja competitiva del cliente" .
En la gestión de proyectos tradicional, cualquier cambio en las últimas etapas se toma con cautela, ya que esto generalmente significa un aumento del alcance y, por lo tanto, costos más altos. Sin embargo, en Agile, los equipos tienen como objetivo aceptar la incertidumbre y reconocer que incluso un cambio tardío aún puede tener mucho valor para el cliente final. Debido a la naturaleza del proceso iterativo de Agile, los equipos no deberían tener problemas para responder a esos cambios de manera oportuna.

| #3 Entrega valor con frecuencia |
| -------------------------- |
El tercer principio de gestión de proyectos Agile establece originalmente, "entregue software que funcione con frecuencia, desde un par de semanas hasta un par de meses, con preferencia a la escala de tiempo más corta" . Su objetivo principal es reducir el tamaño de los lotes que utiliza para procesar el trabajo.

| #4 Rompe los silos de tu proyecto |
| -------------------------- |
Agile se basa en equipos multifuncionales para facilitar la comunicación entre las diferentes partes interesadas en el proyecto. Como dice el texto original, "los empresarios y los desarrolladores deben trabajar juntos a diario durante todo el proyecto".
En un contexto de trabajo del conocimiento que no está explícitamente relacionado con el desarrollo de software, puede cambiar fácilmente la palabra "desarrolladores" a "ingenieros" o "diseñadores" o lo que mejor se adapte a su situación. El objetivo es crear una sincronización entre las personas que crean valor y las que lo planifican o venden. De esta manera, puede hacer que la colaboración interna sea fluida y mejorar el rendimiento de su proceso.

| #5 Construir proyectos alrededor de personas motivadas |
| -------------------------- |
La lógica detrás del quinto de los principios ágiles es que al reducir la microgestión y empoderar a los miembros del equipo motivados, los proyectos se completarán más rápido y con mejor calidad.
Como dice el texto original que sigue al manifiesto Agile, debe " construir proyectos en torno a personas motivadas. Bríndeles el entorno y el apoyo que necesitan, y confíe en ellos para hacer el trabajo".
La segunda oración de este principio es especialmente importante. Si no confía en su equipo y mantiene centralizadas incluso las decisiones más pequeñas en su empresa, solo obstaculizará el compromiso de su equipo. Como resultado, las personas nunca tendrán un sentido de pertenencia al propósito que un proyecto determinado está tratando de cumplir, y no se aprovechará al máximo su potencial.

| #6 La forma más efectiva de comunicación es cara a cara |
| -------------------------- |
"El método más eficiente y efectivo para transmitir información a un equipo de desarrollo y dentro de él es una conversación cara a cara".
En 2001, este principio dio en el clavo. Al comunicarse en persona, reduce el tiempo entre hacer una pregunta y recibir una respuesta. Sin embargo, en el entorno de trabajo moderno donde los equipos colaboran en todo el mundo, ofrece una limitación importante.

| #7 El software funcional es la principal medida de progreso |
| -------------------------- |
El séptimo de los principios básicos de Agile es bastante sencillo. No importa cuántas horas de trabajo haya invertido en su proyecto, cuántos errores haya logrado corregir o cuántas líneas de código haya escrito su equipo.
Si el resultado de tu trabajo no es como tu cliente espera que sea, estás en problemas

|#8 Mantenga un ritmo de trabajo sostenible|
| -------------------------- |
La formulación precisa de este principio es "Los procesos ágiles promueven el desarrollo sostenible. Los patrocinadores, desarrolladores y usuarios deben poder mantener un ritmo constante indefinidamente".
Lógicamente, al poner en práctica Agile, su objetivo es evitar la sobrecarga y optimizar la forma en que trabaja para que pueda entregar con frecuencia al mercado y responder al cambio sin requerir actos heroicos personales de su equipo.

|#9 La excelencia continua mejora la agilidad|
| -------------------------- |
Como afirman los fundadores de Agile Manifesto, " la atención continua a la excelencia técnica y el buen diseño mejoran la agilidad" . En un contexto de desarrollo, este principio permite a los equipos crear no solo software que funcione, sino también un producto estable de alta calidad.
Como resultado, será menos probable que los cambios en el código afecten negativamente a los errores y fallas.
Aún así, el noveno de los principios de gestión Agile es aplicable en todas las industrias. Cuando mantenga la excelencia operativa, tendrá menos problemas para reaccionar a los cambios y mantener la agilidad.

|#10 La simplicidad es esencial|
| -------------------------- |
El contenido original de este principio puede resultar un poco confuso, ya que afirma:  " La simplicidad, el arte de maximizar la cantidad de trabajo no realizado, es esencial" . Sin embargo, es muy práctico.
Si puedes hacer algo de forma sencilla, ¿por qué perder el tiempo complicándolo? Tus clientes no pagan por la cantidad de esfuerzo que inviertes. Están comprando una solución a un problema específico que tienen. Tenga eso en cuenta cuando implemente Agile y evite hacer algo solo por hacerlo.

|#11 Los equipos autoorganizados generan el mayor valor|
| -------------------------- |
Una vez más, nos damos cuenta de que cuando se les brinda libertad, los equipos motivados generan el mayor valor para el cliente. Al discutir este principio, los 17 padres de Agile afirmaron que "las mejores arquitecturas, requisitos y diseños surgen de equipos autoorganizados".

|# 12 Reflexione y ajuste regularmente su forma de trabajar para aumentar la efectividad|
| -------------------------- |
Finalmente, hemos llegado al último de los principios de gestión Agile. Está relacionado con la evaluación de su desempeño y la identificación de margen de mejora. La versión larga del principio establece: "A intervalos regulares, el equipo reflexiona sobre cómo volverse más efectivo, luego sintoniza y ajusta su comportamiento en consecuencia" .

# PSI Plan de Sietma de Información









To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Nicky-nn/ingenieria-del-software.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/Nicky-nn/ingenieria-del-software/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
